import firebrowse
import json
import urllib
import tarfile
#import csv
#import sys
#from numpy import genfromtxt
#import argparse
#import pandas
#from sklearn.decomposition import PCA
#import matplotlib.pyplot as plt
#import numpy as np
import os
import shutil
import gzip

data_file_dir = "data"
cnv_tool = "Merge_snp__genome_wide_snp_6__broad_mit_edu__Level_3__segmented_scna_hg19__seg"
rna_tool = "Merge_rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes__data"


#including FPPP gets error
datasets = ("ACC","BLCA","BRCA","CESC","CHOL","COAD","COADREAD","DLBC","ESCA","GBM","GBMLGG","HNSC","KICH","KIPAN","KIRC","KIRP","LAML","LGG","LIHC","LUAD","LUSC","MESO","OV","PAAD","PCPG","PRAD","READ","SARC","SKCM","STAD","STES","TGCT","THCA","THYM","UCEC","UCS","UVM")

#check_if_exists = True



for dataset in datasets:    
    cnv_file_name = os.path.join(data_file_dir,"CNV_" + dataset + ".txt.gz")
    
    if not os.path.exists(cnv_file_name):
        filename_cnv = os.path.join(data_file_dir,dataset+"_gep_"+".tar.gz")

        # germline subtracted variants is way lower.. when compared to cbioportal signal 
        # convinced that the cbioportal data is without germline subtraction

        c = firebrowse.Archives().StandardData(format="json", cohort=dataset, data_type="CopyNumber", page_size="100", tool=cnv_tool, level="3")

        json_data = json.loads(c)["StandardData"]

        # TODO automatize/customize FFPE vs FF data fetch
        # first element has FFPE data, second (1) has fresh frozen data
        default_type = "frozen"
        for i_json in range(0,len(json_data)):
            if json.loads(c)["StandardData"][i_json]['sample_prep'] == default_type:
                cnv_url = json.loads(c)["StandardData"][i_json]['urls'][0]
                break
        
        #l = len(json_data)
        #fetch = len(json_data) - 1
        #fetch = i_json

        #cnv_url = json.loads(c)["StandardData"][fetch]['urls'][0]
        urllib.urlretrieve(cnv_url, filename_cnv)

        tar = tarfile.open(filename_cnv)
        n = tar.getnames()
        tar.extractall(path=data_file_dir)
        tar.close()
        os.remove(filename_cnv)

        seg_txt_file_name = os.path.join(data_file_dir, filter(lambda x:'seg.txt' in x, n)[0])
        
        with open (seg_txt_file_name, "rb") as file_in, gzip.open(cnv_file_name, "wb") as file_out:
            shutil.copyfileobj(file_in, file_out)
            

        print dataset + " CNV downloaded"    
    else:
        print dataset + " CNV already exists"       
        
        
    rna_file_name = os.path.join(data_file_dir, "mRNA_" + dataset + ".txt.gz")

    if not os.path.exists(rna_file_name):
        filename_gep = os.path.join(data_file_dir,dataset+"_gep_"+".tar.gz")

        # fetch RNA-seq processed data and extract tarball 
        r = firebrowse.Archives().StandardData(format="json", cohort=dataset, data_type="mRNAseq", page_size="100", protocol="RSEM_genes", tool=rna_tool, level="3")

        gep_url = json.loads(r)["StandardData"][0]['urls'][0]
        urllib.urlretrieve (gep_url, filename_gep)

        tar = tarfile.open(filename_gep)
        n = tar.getnames()
        tar.extractall(path=data_file_dir)
        tar.close()
        os.remove(filename_gep)
        
        data_txt_file_name = os.path.join(data_file_dir, filter(lambda x:'data.txt' in x, n)[0])
        
        with open (data_txt_file_name, "rb") as file_in, gzip.open(rna_file_name, "wb") as file_out:
            shutil.copyfileobj(file_in, file_out)

        print dataset + " mRNA downloaded"
    else:
        print dataset + " mRNA already exists"
        
    for file_in_data in os.listdir(data_file_dir):
        if cnv_tool in file_in_data or rna_tool in file_in_data:
            shutil.rmtree(os.path.join(data_file_dir, file_in_data))
