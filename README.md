# ICAro
Inferring gene signature from Cancer copy number Aberrations

The intended purpose of this package is to extract a gene signature from public cancer genomic data (e.g., the TCGA), by focusing on focal deletions and RNA-seq. The tool is based on Broad Institute's firebrowse Python Package.

